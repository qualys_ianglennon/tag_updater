# tag_updater.py

## Description

The tag_updater.py script takes 3 arguments to specify client account, IP list and Tag ID.

An XML object is created to serve as the ServiceRequest input to the Update Tag API call from the Qualys Asset
Management & Tagging API.

Example:
```text
https://qualysapi.qualys.com/qps/rest/2.0/update/am/tag/12345678
``` 
```xml
<ServiceRequest>
  <data>
    <Tag>
      <ruleText>192.168.0.1, 192.168.0.5, 192.168.0.250, 192.168.2.0/24</ruleText>
    </Tag>
  </data>
</ServiceRequest>
```

The script will also accept two optional arguments to enable debugging on the API call or to simulate the API call 
by building the URL string and XML object from the provided inputs and printing those objects to stdout.

## Usage
```text
$ python3.7 tag_updater.py -h
usage: tag_updater.py [-h] [--simulate] [-d] client iplist tagid

positional arguments:
  client          Client name as shown in config.ini section
  iplist          File containing comma-separated list of IP addresses
  tagid           Tag ID to update

optional arguments:
  -h, --help      show this help message and exit
  --simulate, -s  Do not make the API call, instead print the URL and XML
                  objects to stdout
  -d, --debug
```

## config.ini
The configuration file contains API connectivity details (username, password, API server, proxy details) 
necessary to make API calls.  The file is broken into Sections where each section title corresponds to a 
client name or identifier.  This name or identifier is used as the 'client' argument to the script.

Example:
```text
[DEFAULT]
APIServer=https://qualysapi.qualys.eu
APIUser=username
APIPassword=password

APIProxyAddress=https://proxy.address.local
APIProxyEnable=No

[foo]
APIServer=https://qualysapi.qualys.eu
APIUser=username
APIPassword=password

APIProxyAddress=https://proxy.address.local
APIProxyEnable=No
```

In this example two sections are shown, 'DEFAULT' and 'foo'.
