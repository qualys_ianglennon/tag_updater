import QualysAPI
import configparser
import argparse
import xml.etree.ElementTree as ET
import sys

def createServiceRequest(ruletext):
    sr = ET.Element('ServiceRequest')
    data = ET.SubElement(sr, 'data')
    tag = ET.SubElement(data, 'Tag')
    rt = ET.SubElement(tag, 'ruleText')
    rt.text = ruletext

    return sr

if __name__ == '__main__':

    # Setup script arguments parser using argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('client', help='Client name as shown in config.ini section')
    parser.add_argument('iplist', help='File containing comma-separated list of IP addresses')
    parser.add_argument('tagid', help='Tag ID to update', type=int)
    parser.add_argument('--simulate', '-s', action="store_true", help='Do not make the API call, instead print the URL '
                                                                      'and XML objects to stdout')
    parser.add_argument('-d', '--debug', action="store_true")
    args = parser.parse_args()

    # Setup and read the configuration file
    configfile = 'config.ini'
    config = configparser.ConfigParser()
    config.read(configfile)

    if args.client not in config.sections():
        print("ERROR: Client '%s' not found in %s" % (args.client, configfile))
        sys.exit(1)

    apiserver = config[args.client]['APIServer']
    apiuser = config[args.client]['APIUser']
    apipasswd = config[args.client]['APIPassword']

    apiproxyenable = False
    apiproxyaddr = ''
    if config[args.client]['APIProxyEnable'] == 'Yes' or config[args.client]['APIProxyEnable'] == 'True':
        apiproxyenable = True
        apiproxyaddr = config[args.client]['APIProxyAddress']

    api = QualysAPI.QualysAPI(svr=apiserver, usr=apiuser, passwd=apipasswd, enableProxy=apiproxyenable,
                              proxy=apiproxyaddr, debug=args.debug)

    ipfile = open(args.iplist, 'r')
    ips = ipfile.read()
    ipfile.close()

    sr = createServiceRequest(ips)
    url = '%s/qps/rest/2.0/update/am/tag/%s' % (api.server, str(args.tagid))
    payload = ET.tostring(sr, encoding='utf-8', method='xml').decode()
    if args.simulate:
        print("%s\n\n%s\n\n" % (url, payload))
    else:
        api.makeCall(url=url, payload=payload)
